/**
 * 選手
 */
public class Player{
    /**
     * 氏名
     */
    private String name;

    /**
     * 年齢
     */
    private int age;

    /**
     * チーム
     */
    private Team team;
    
    /**
     * コンストラクタ
     * @param name
     * @param age
     * @param team
     */
    public Player(String name, int age, Team team){
        this.name = name;
        this.age  = age;
        this.team = team; 
    }
    // getter / setter , toString, equals, hashCodeは自分で記述してください。
    // Lombokを使うのもOKです。
}