import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Mainクラス
 */
public class MainApplication{
    public static void main(String[] args) {
        Player yamada = new Player("yamada", 21, Team.TIGERS);
        Player tanaka = new Player("tanaka", 19, Team.GIANTS);
        Player suzuki = new Player("suzuki", 25, Team.DRAGONS);
        Player koyama = new Player("koyama", 30, Team.HIROSHIMA);
        Player harada = new Player("harada", 28, Team.RAKUTEN);
        Player tanabe = new Player("tanabe", 31, Team.DENA);

        List<Player> players = Arrays.asList(yamada, tanaka, suzuki, koyama, harada, tanabe);
        
        // 選手の年齢が若い順に並べ替えて、出力してください。
        
    }
}