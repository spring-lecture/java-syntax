/**
 * 従業員
 */
public class Employee{
    /**
     * 氏名
     */
    private String name;

    /**
     * 給料
     */
    private int salary;

    /**
     * コンストラクタ
     * @param name
     * @param salary
    */
    public Employee(String name, int salary){
        this.name = name;
        this.salary  = salary;
    }
    // getter / setter , toString, equals, hashCodeは自分で記述してください。
    // Lombokを使うのもOKです。
}