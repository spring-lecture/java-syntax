/**
 * 東京証券取引所の市場区分
 */
public enum TseSection{
    FIRST, SECOND, MOTHERS, JASDAQ;
}