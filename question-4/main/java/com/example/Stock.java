/**
 * 株式
 */
public class Stock{
    /**
     * 会社名
     */
    private String companyName;

    /**
     * 証券取引所　上場市場
     */
    private TseSection tseSection;

    /**
     * 株価
     */
    private int price;

    /**
     * コンストラクタ
     * @param companyName
     * @param tseSection
     * @param price
     */
    public Stock(String companyName, TseSection tseSection, int price){
        this.companyName = companyName;
        this.tseSection  = tseSection;
        this.price   = price;
    }
    // getter / setter , toString, equals, hashCodeは自分で記述してください。
    // Lombokを使うのもOKです。
}