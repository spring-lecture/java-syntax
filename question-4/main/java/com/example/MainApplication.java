import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Mainクラス
 */
public class MainApplication{
    public static void main(String[] args) {
        Stock softbank = new Stock("softbank", TseSection.FIRST,   4674);
        Stock zozo     = new Stock("zozo",     TseSection.FIRST,   2139);
        Stock nintendo = new Stock("nintendo", TseSection.FIRST,   43200);
        Stock toshiba  = new Stock("toshiba",  TseSection.SECOND,  3805);
        Stock mixi     = new Stock("mixi",     TseSection.MOTHERS, 2057);
        Stock mcdonald = new Stock("mcdonald", TseSection.JASDAQ,  5270);

        List<Stock> stocks = Arrays.asList(softbank, zozo, nintendo, toshiba, mixi, mcdonald);

        // 東証一部であり、かつ株価が3,000円以上である会社を出力
        
    }
}